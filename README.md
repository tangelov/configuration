# own-config
Este repositorio va a contener los playbooks necesarios para realizar una configuración básica de los PCs que utilizo con GNU/Linux.

Tras sufrir la pérdida de una instalación y ver que pese lo que más pereza me daba era reinstalarlo
todo, no recuperar la información de mis backups, decidí empezar a codificar mis necesidades. Espero que este código sea lo suficiente modular para poder ser utilizado por otros.

## Necesidades
Como técnico de sistemas y administrador de sistemas en la nube existen algunas aplicaciones y utilidades que suelo instalar en mis sistemas:

* Algunas utilidades de consola como _git_, _wget_, _curl_, _vim_ o _tmux_

* Las CLIs de los diferentes proveedores en la nube que utilizo (Microsoft Azure, Amazon Web Services y Google Cloud Platform).

* Algunas utilidades de terceros para realizar mi trabajo y mis investigaciones (como _terraform_, _docker_ o _kubectl_).

* Siempre organizo mi trabajo en dos carpetas: una de utilidades y otra de desarrollos.

* También suelo utilizar Gnome Shell y ciertas herramientas gráficas, que gestiono a través de repositorios de paquetes, Snap y Flatpack


## Configuración 
La estructura de carpetas y ficheros del repositorio es la siguiente:

```bash
├── launcher.sh
├── main.yml
├── vars.yml
├── vault.yml
├── requirements.yml
├── README.md
└── playbooks
    └──tasks
        ├── apps
        │   ├── flatpak.yml
        │   └── snap.yml
        ├── backups.yml
        ├── common.yml
        ├── devops-cloud
        │   ├── cloud.yml
        │   ├── hashicorp.yml
        │   ├── requirements.txt
        │   └── virtualbox.yml
        ├── gnome-conf.yml
        └── others.yml
```

Cada fichero tiene ciertas funciones definidas:

* _launcher.sh_ es un instalador de dependencias pensado para instalar todas las necesidades que tiene Ansible en Python 3. Las dependencias de los playbooks se instalan gracias al fichero _requirements.yml_ en este paso.

* _main.yml_ funciona como orquestador del resto de servicios.

* _vars.yml_ y _vault.yml_ son los ficheros donde se almacenan las variables con las prácticas recomendadas por Ansible.

* La carpeta _playbooks_ almacena las colecciones de tareas que son ejecutadas por host. Cada playbook tiene como un nombre definido en función del host que vamos a controlar.

* La carpeta _tasks_ almacena todas las tareas agrupadas por temática:

  * _apps_ contiene la configuración de las aplicaciones gestionadas a través de Snap o Flatpak.

  * _devops-cloud_ contiene toda la configuración de las aplicaciones profesionales (proveedores cloud, herramientas de hashicorp y otros).

  * _backups.yml_ contiene toda la configuración del sistema de backups.

  * _common.yml_ contiene la configuración básica que se aplicará (paquetes iniciales y prerrequisitos de consola).

  * _gnome-conf.yml_ contiene la configuración gráfica de Gnome: extensiones y modificaciones en la interfaz gráfica.

  * _others.yml_ ofrece la posibilidad de instalar aplicaciones y paquetes extra.


## Uso de submódulos
El sistema de configuración permite la utilización de un submódulo para obtener algunos ficheros. El submódulo por defecto es mío y contiene algunos ficheros cifrados, como vaults y ciertas variables que aunque podrían estar en el repositorio público, prefiero _oscurecerlos_.

Si no queremos utilizar un repositorio _de secretos_, recomendamos hacer un fork de este proyecto y ejecutar ```git submodule deinit --all```. También tendremos que modificar el fichero _launcher.sh_ y comentar ciertas partes del fichero, junto a modificar en el fichero _vars.yml_ la variable _extra\_configuration_ a __false__.

Si queremos utilizar un repositorio _de secretos_ diferente, el procedimiento es ligeramente diferente. Debemos crear un repositorio con al menos la siguiente estructura:

```bash
├── public.key
├── tasks
│   └── apps
├── trust.txt
└── vaults
```

Voy a explicar ligeramente que sería cada carpeta y fichero:

* __public.key__ y __trust.txt__ son la parte pública de la clave GPG que utilizamos para cifrar los backups y el fichero de _confianza_ para que confiemos automáticamente en dicha clave.

* __tasks/apps/__ debe contener los ficheros _flatpak.yml_, _snap.yml_ y _others.yml_ que tienen correspondientemente aplicaciones en flatpak, snap y repositorios tradicionales.

* __vaults/__ debe contener los vaults de Ansible que vayamos a utilizar con un formato $HOSTNAME-vault.yml.


## Utilización
Siguiendo las recomendaciones de Ansible, las variables se definen en _vars.yml_ y referencian a variables declaradas en _vault.yml_. La gestión de los playbooks se realiza a través de los dos ficheros de variables:

* Para habilitar servicios y configuraciones debemos utilizar el fichero _vars.yml_.

* Para configurar servicios y aplicaciones debemos de editar el fichero encriptado _vault.yml_ utilizando el comando _ansible-vault_.

### Creación de un almacén de claves (vault)
Aunque este playbook no gestiona credenciales en si mismo, puede contenerlas y por ello es necesario crear un almacén de claves de Ansible. Podemos acceder a toda la documentación sobre su creación [aquí](https://docs.ansible.com/ansible/latest/user_guide/vault.html)

La forma más básica de crearlo es ejecutando _ansible-vault create vault.yml_ e introduciendo una contraseña.

### Lanzamiento del playbook

Estos playbooks se pueden utilizar para configurar múltiples hosts. Aunque están pensados principalmente para máquinas de escritorio, es posible que queramos utilizarlo contra otras máquinas (por ejemplo para aplicar su sistema de backups de forma uniforme).

Para hacerlo debemos hacer dos cosas:

* Crear un fichero de inventario en la raíz de la carpeta donde hallamos clonado este repositorio.

* Creamos una carpeta llamada _host\_vars_ y creamos un fichero de variables por cada host con el nombre del mismo. Si nuestro host se llama raspi, creamos el fichero _raspi.yml_ o _raspi_. También podemos modificar el fichero _main.yml_ y añadir los ficheros de variables que queramos en _vars\_files_.

* Lanzar el playbook con las tags adecuadas en función de lo que queramos instalar y con el parámetro _--limit=$nombredelhost_ para que se ejecute sólo en dicho host.

Adjuntamos un inventario a modo de ejemplo, pero si se desea más información, nos remitimos a la [documentación oficial](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html). También podríamos utilizar un inventario dinámico.

```ini
[all]
localhost    ansible_connection=local
raspi        ansible_connection=ssh          ansible_user=pi    ansible_host=172.16.16.100
```

Esta podría ser la estructura de carpetas del inventario a modo de ejemplo:

```bash
.
├── host_vars
│   ├── raspi.yml
│   └── localhost.yml
├── main.yml
└── inventory

```

Con dicho inventario, si quisiesemos aplicar sólo la parte de backup en nuestra raspi, tendríamos que ejecutar el siguiente comando

```bash
ansible-playbook -i inventory main.yml --tags backups,common --ask-become-pass --ask-vault-pass --extra-vars "my_ansible_host=raspi"
```
En este caso concreto, nos preguntaría la contraseña de sudo y la contraseña para acceder a nuestro Ansible Vault y aplicaría las tareas comunes (configuración de las CLIs) y las de backup.


## Variables

| Variables   |      Descripción      |  Tipo |
|----------|:-------------|:------|
| default\_shell | Shell a utilizar por defecto en nuestro usuario | String |
| default\_shell\_config | Diccionario con los ficheros de configuración básicos de cada shell | Dict |
| oh\_my\_zsh\_config | Diccionario para configurar la instalación de Oh My ZSH y sus plugins. Su estructura puede consultarse [aquí](https://github.com/gantsign/ansible-role-oh-my-zsh) | Dict |
| dev\_folder |  Ruta completa donde se localiza la carpeta de desarrollo | String |
| tools\_folder |    Ruta completa donde se localiza la carpeta de tools   |   String |
| gnome\_shell\_enabled | Flag para habilitar la personalización de Gnome Shell |    Boolean |
| gnome\_shell\_ext\_ids | Identificadores de las extensiones de Gnome Shell a instalar. Su estructura puede consultarse [aquí](https://github.com/jaredhocutt/ansible-gnome-extensions) | List |
| flatpak\_enabled | Flag para habilitar la instalación de aplicaciones en Flatpak | Boolean |
| snap\_enabled | Flag para habilitar la instalación de aplicaciones en Snap | Boolean |
| terraform\_version | Versión de Terraform a instalar desde la web de Hashicorp | String |
| vagrant\_version | Versión de Vagrant a instalar desde la web de Hashicorp | String |
| packer\_version | Versión de Packer a instalar desde la web de Hashicorp | String |
| awscli\_enabled | Flag para habilitar la instalación de la CLI de AWS | Boolean |
| awscli\_version | Versión de awscli a instalar para gestionar AWS | String |
| azurecli\_enabled | Flag para habilitar la instalación de las CLIs de Azure | Boolean|
| gcpcli\_enabled | Flag para habilitar la instalación de las CLIs de GCP | Boolean |
| docker\_extra\_utils\_enabled | Flag para habilitar la instalación de las tools extra para contenedores y Kubernetes | Boolean |
| podman\_compose\_version | Versión de Podman Compose a instalar desde PyPi | String |
| extra\_configuration | Flag para habilitar o no el uso de configuraciones extra en submódulos | Boolean |
| kubectl\_version | Versión de kubectl a instalar para gestionar K8s | String |
| dive\_version | Versión de dive a instalar para analizar contenedores | String |
| helm\_version | Versión de helm a instalar para gestionar aplicaciones en K8s | String |
| hugo\_version | Versión de hugo a instalar para generar contenido estático | String |
| opa\_version | Versión de OPA a instalar para generar testing de estáticos de IaC y configuración | String |
| backup\_enabled | Flag para habilitar el sistema de backups del playbook | Boolean |
| ninja\_\* | Todas las variables que empiecen por ninja\_ se encuentran explicadas en su correspondiente [rol](https://gitlab.com/tangelov-roles/backupninja) | No aplica |
| rclone\_version | Versión de rclone que deseamos instalar | String |
| rclone\_remote\_name | Nombre del remote en la nube que vamos a crear para sincronizar nuestros backups | String |
| rclone\_cron\_hour | Hora a la que se va a ejecutar nuestro cron de sincronización con rclone | String |
| rclone\_cron\_minute | Minutos a los que se va a ejecutar nuestro cron de sincronización con rclone | String |


## Backups locales y remotos
Este playbook permite la generación de backups diarios locales y en remoto. En el caso de utilizar backups remotos, utilizamos [rclone](https://rclone.org/). Recomendamos leer la documentación oficial sobre cómo conectarse a los diferentes proveedores (como [Google Drive](https://rclone.org/drive/), [Nextcloud](https://rclone.org/webdav/#nextcloud), [Amazon S3](https://rclone.org/s3/) o [Microsoft OneDrive](https://rclone.org/onedrive/)) para configurar correctamente los remotos. __Este playbook no configura los remotos__.


## TODO

* Revisar [esto](https://github.com/PeterMosmans/ansible-role-customize-gnome)
