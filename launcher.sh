#!/bin/bash

# Installation of Python3 and Pip3
echo "Ansible Prerrequisites installation"
sudo apt update && sudo apt install python3-pip git jq -y

pip3 install -r requirements.txt --user --break-system-packages

## Download and Installation of Sops
export SOPS_VERSION="3.9.4"

wget "https://github.com/getsops/sops/releases/download/v${SOPS_VERSION}/sops_${SOPS_VERSION}_amd64.deb"
sudo apt install "./sops_${SOPS_VERSION}_amd64.deb" -y
rm "sops_${SOPS_VERSION}_amd64.deb"

# Reloading files from bash
source ~/.bashrc
source ~/.profile

# Installing Ansible dependencies
ansible-galaxy collection install -r requirements.yml --force --pre
ansible-galaxy role install -r requirements.yml --force

# Adding Extra configuration files
# Comment this if not needed
echo ""
echo "Updating submodule"
git submodule init && git submodule update --remote

# Copying inventory from secret repository
# Comment this if not used
echo "Moving inventory files to final destination"
cp -r temp/ansible/* .

# Copying Apps files to final destination
# Comment this if not used
echo "Moving app.yml files to final destination"
cp temp/tasks/apps/flatpak.yml playbooks/tasks/apps/flatpak.yml
cp temp/tasks/apps/snap.yml playbooks/tasks/apps/snap.yml
cp temp/tasks/apps/others.yml playbooks/tasks/others.yml
