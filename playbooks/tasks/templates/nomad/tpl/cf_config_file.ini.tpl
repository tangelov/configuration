# Cloudflare API token used by Certbot
dns_cloudflare_api_token = "{{ with nomadVar "nomad/jobs" }}{{ .cloudflare_dns_access_token }}{{ end }}"
