#!/bin/sh

# Note, we're not dealing with bash here. We're dealing with 'ash', as this docker image is BusyBox linux
# Hence, we can't use normal bash arrays. Separate each domain by SPACES, NOTE COMMAS
DOMAINS="{{ with nomadVar "nomad/jobs" }}{{ .domains }}{{ end }}"

# Set this to an empty string "" for production mode
STAGING="" # --staging 

DNS_PROPAGATION_SECONDS=60

set -- $DOMAINS

while [ -n "$1" ]; do
  domain=$1
  echo "Domain: $domain"

  certbot certonly -v $STAGING --cert-name ${domain} -d ${domain} --agree-tos -m {{ with nomadVar "nomad/jobs" }}{{ .email }}{{ end }} --keep-until-expiring --non-interactive --dns-cloudflare --dns-cloudflare-credentials /alloc/etc/letsencrypt/cloudflare-certbot-config.ini --dns-cloudflare-propagation-seconds ${DNS_PROPAGATION_SECONDS}

  echo "Done with: $domain"
  shift
done
