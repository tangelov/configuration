---
- hosts: "{{ my_ansible_host }}"

  tasks:
  - name: Installation of basic utils
    tags: common
    ansible.builtin.import_tasks: tasks/common.yml

  - name: Installation of common web dependencies
    become: true
    tags: webserver
    block:
      - name: Installation of PHP dependencies
        tags: php
        ansible.builtin.import_tasks: tasks/php.yml

      - name: Importing of Nginx and Letsencrypt tasks
        ansible.builtin.import_tasks: tasks/web.yml

      - name: Installation of MySQL dependencies
        tags: mysql
        ansible.builtin.import_tasks: tasks/mysql.yml

  - name: Installing RPI Cluster stack
    become: true
    tags: rpi-cluster
    block:
      - name: Folders to store files in the disk instead of the SD
        ansible.builtin.file:
          path: "{{ mount_point }}/{{ item }}"
          owner: "root"
          group: "root"
          mode: 0755
          state: directory
        loop:
          - "docker"
          - "host_logs"
          - "tmp"

      - name: Installation of Docker Daemon stack
        ansible.builtin.import_tasks: tasks/rpi-cluster.yml

      - name: Installation of Consul stack
        tags: consul
        ansible.builtin.import_tasks: tasks/consul.yml

      - name: Installation of Nomad stack
        tags: nomad
        ansible.builtin.import_tasks: tasks/nomad.yml

      - name: Deploying Nomad-based workload configurations
        tags: nomad
        ansible.builtin.import_tasks: tasks/nomad-workloads.yml

  - name: Install VPN clients
    tags: vpn
    block:
      - name: Installing VPN clients
        ansible.builtin.import_tasks: tasks/tailscale.yml

  - name: Installing Nextcloud
    become: true
    tags: nextcloud
    block:
      - name: Creating directory to mount disk
        ansible.builtin.file:
          path: "{{ mount_point }}"
          owner: "root"
          group: "root"
          mode: 0755
          state: directory

      - name: Mounting disk and adding to fstab
        ansible.posix.mount:
          path: "{{ mount_point }}"
          src: "{{ mount_disk }}"
          fstype: "{{ mount_disk_fs }}"
          state: mounted

      - name: Installation of Nextcloud
        tags: nextcloud
        ansible.builtin.import_tasks: tasks/nextcloud.yml

  - name: Installing Pihole
    become: true
    tags: pihole
    block:
      - name: Installation of Pihole
        ansible.builtin.import_tasks: tasks/pihole.yml

  - name: Enabling monitoring system
    become: true
    tags: monitor
    block:
      - name: Installation and configuration of Grafana Cloud agent
        ansible.builtin.import_tasks: tasks/monitor.yml

  - name: Enabling backup systems
    become: true
    tags: backups
    block:
      - name: Enabling local backups
        ansible.builtin.import_tasks: tasks/backups.yml
    when: backups_enabled

  - name: Upgrade all packages and services
    become: true
    tags: maintenance
    block:
      - name: Stopping services pre-upgrade
        ansible.builtin.systemd:
          name: "{{ item }}"
          state: stopped
          enabled: yes
        loop:
          - "nginx"
          - "php{{ php_version }}-fpm"
          - "mysqld"

      - name: Update system packages
        ansible.builtin.apt:
          upgrade: safe
          autoremove: yes

      - name: Checking if reboot is needed
        ansible.builtin.stat:
          path: "/var/run/reboot-required"
        register: rb_boolean

      - name: Rebooting server if needed
        block:
          - name: Stopping services in correct order
            ansible.builtin.systemd:
              name: "{{ item }}"
              state: stopped
              enabled: yes
            loop:
              - "nginx"
              - "php{{ php_version }}-fpm"
              - "mysqld"

          - name: Restarting instance
            ansible.builtin.shell: "sleep 5 && reboot"
            async: 1
            poll: 0

          - name: Wait for connection
            ansible.builtin.wait_for_connection:
              delay: 60
              timeout: 300
        when: rb_boolean.stat.exists

      - name: Ensuring services are started
        ansible.builtin.systemd:
          name: "{{ item }}"
          state: started
          enabled: yes
        loop:
          - "mysqld"
          - "php{{ php_version }}-fpm"
          - "nginx"

  handlers:
  - name: Import all handlers
    become: true
    ansible.builtin.import_tasks: tasks/handlers.yml
  