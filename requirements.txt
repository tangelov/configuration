ansible==11.2.0
python-gitlab==5.1.0
pytest==8.3.4
testinfra==6.0.0
netaddr==1.3.0
jmespath==1.0.1
