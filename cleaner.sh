#!/bin/bash

# Cleanup of custom folders
echo "Cleanup of custom files in the repository"

CUSTOM="flatpak snap others"

for i in $CUSTOM
do
    if [[ -f "playbooks/${i}-prod.yml" ]]; then
        case $i in
            flatpak|snap)
                git checkout playbooks/tasks/apps/${i}.yml
                ;;
            *)
                git checkout playbooks/tasks/${i}.yml
                ;;

        esac
	echo "${i}.yml cleaned."
    fi
done
